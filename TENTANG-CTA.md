---
Title: "Tentang CTA untuk publikasi akademik"
Author: "Dasapta Erwin Irawan"
Affil: "Institut Teknologi Bandung"
---

# Tentang Copyright Transfer Agreement (CTA)

Oleh: Dasapta Erwin Irawan (ITB / RINarxiv) | [ORCID](https://orcid.org/0000-0002-1526-0863)

Lisensi dokumen: [CC-0 public domain](https://creativecommons.org/publicdomain/zero/1.0/)

Sebelum jauh, **saya bukan ahli hukum**, jadi yang saya sampaikan di sini baru sampai pada tahap menelaah kelogisan bahwa Hak Cipta perlu atau harus dipindahkan dari pemiliknya ke penerbit sebelum diterbitkan. Perlu banyak ahli hukum yang menelaah tentang ini. Di sini perlu ditekankan bahwa penerbit tidak berperan besar dalam penyusun manuskrip. Proses riset yang sebelumnya terjadi (pengambilan data, analisis, dan pelaporan) juga merupakan upaya besar penulis, bukan penerbit.  

# Apakah CTA itu?
CTA yang dimaksud dengan CTA di sini adalah copyright transfer agreement, yakni sebuah perjanjian yang mengatur hak dan kewajiban penulis dan penerbit. CTA berbentuk surat berisi beberapa klausul yang harus ditandatangani oleh penulis atau perwakilan penulis, biasanya oleh penulis korespondensi.

# Bagaimana awal mula CTA?
Saya belum mendapatkan rujukan yang tepat untuk menjawab ini, tapi saya membayangkan bahwa surat ini mungkin warisan dari dominasi penerbit yang zaman dulu menjadi otoritas penerbitan buku ilmu pengetahuan. Karena dominasi itu, maka ide apapun yang dimiliki penulis hanya akan terwujud menjadi bahan tercetak yang dapat dibaca orang lain, hanya bila dicetak oleh penerbit.

Sebuah laman [Wikipedia tentang CTA](https://en.wikipedia.org/wiki/Copyright_transfer_agreement) telah tersedia dan sedikit menceritakan sejarah CTA. Menurut laman itu, proses pengalihan Hak Cipta marak terjadi sejak terbitnya  UU Hak Cipta AS dan tahun 1976. Diceritakan bahwa proses pengalihan Hak Cipta dari penulis ke penerbit dibutuhkan, agar penerbit dapat menjual karya penulis. [Tennant (2018)](http://fossilsandshit.com/ethics-copyright-transfer-scientific-research/) dalam artikel blognya juga telah menjelaskan keanehan yang ada dalam CTA dan bahwa banyak penulis yang abai terhadap hal tersebut.

# Kapan penulis akan diminta menandatangani CTA?
Penulis akan disodori surat CTA ini setelah makalah dinyatakan diterima (accepted). Setelah ditandatangani dan dikirim kembali ke penerbit, biasanya dalam waktu kurang dari dua minggu, makalah akan tayang perdana secara daring. 

Sebuah tulisan dari [Gadd dkk (2003)](https://onlinelibrary.wiley.com/doi/abs/10.1087/095315103322422053) menjelaskan bahwa dari 80 jurnal akademik yang diobservasinya, 90% meminta penulis untuk mengalihkan Hak Ciptanya dan 69% diantaranya memintanya sebelum proses peninjauan sejawat, sebagian besar membolehkan pengarsipan mandiri tapi tanpa ketentuan yang jelas, dan sangat sedikit jurnal yang menganggap bahwa pengarsipan mandiri adalah bentuk publikasi ganda. Kesimpulan dari tulisan tersebut adalah bahwa CTA perlu direformat ulang agar lebih mencerminkan kepentingan penulis dan penerbit.


# Apakah penulis wajib menandatangani CTA?
Anda bukanlah yang pertama menanyakannya. Di internet, anda dapat mencari beberapa diskusi terkait ini, salah satunya di [StackExchange](https://academia.stackexchange.com/questions/52002/do-i-have-to-sign-a-copyright-transfer-agreement-to-publish-an-article) (Catatan: Percakapan di grup itu terjadi pada bulan Agustus 2015 menandakan bahwa tulisan saya ini bukanlah yang pertama. Jauh sebelumnya pasti sudah ada yang mulai kritis tentang CTA).

Penerbit memang jarang menggunakan kata `harus`, tetapi bila surat tidak ditandatangani, maka proses publikasi akan berhenti. Pihak penerbit pasti akan menanyakan kenapa tidak kunjung menerima surat tersebut dari anda.


# Apa saja klausul dalam CTA?

Klausul yang ada dalam CTA akan terbagi ke dalam dua kelompok besar, yaitu CTA untuk makalah non OA (non _open access_) dan CTA untuk makalah OA. Sebelum melanjutkan, ada baiknya anda membaca tulisan saya tentang [model pengelolaan jurnal ilmiah](http://dasaptaerwin.net/wp/2017/02/pengelolaan-jurnal-ilmiah-konvensional-vs-open-access-bagian-1.html) (ada 4 bagian). Saya akan ambil sampel tiga penerbit besar saja, Elsevier (EL), Springer Nature (SN), dan Taylor Francis (TF). CTA dari penerbit yang lain tidak akan jauh berbeda, walaupun pasti ada variasi.

## CTA untuk rute non OA

Klausul untuk jurnal non OA menurut saya adalah yang paling tidak masuk akal, bila ditinjau dari proporsi upaya maksimum yang dikeluarkan oleh penulis (sejak riset hingga manuskrip dikirimkan ke jurnal) vs upaya minimum dari penerbit (sejak manuskrip diterima sampai terbit).

>In order for Elsevier to publish and disseminate research articles, we need publishing rights. This is determined by a publishing agreement between the author and Elsevier. This agreement deals with the transfer or license of the copyright to Elsevier and authors retain significant rights to use and share their own published articles. [referensi](https://www.elsevier.com/about/policies/copyright) 

### Hak penulis yang beralih ke penerbit

* **The exclusive right** to publish and distribute an article, and to grant rights to others, including for commercial purposes.: dapat dilihat bahwa EL mendapatkan Hak Cipta secara eksklusif untuk menerbitkan dan mendistribusikan artikel penulis. Selain itu mereka juga mendapatkan hak untuk mengkomersialkannya, bukan hanya untuk mereka, tetapi juga untuk pihak ketiga yang bekerjasama dengan mereka. Di sini tidak terlihat ada ketentuan bahwa EL wajib minta izin kepada penulis sebelum mereka bekerjasama dengan pihak ketiga. Jelas karena EL telah mendapatkan hak eksklusif dari anda. Ini semacam anda ingin jual rumah menggunakan jasa agen properti. Saat mereka minta `hak ekslusif` artinya hanya mereka yang boleh menjual rumah anda. Bahkan andapun sebagai pemilik tidak boleh mencari pembeli, kalaupun ada calon pembeli dari anda, maka si calon pembeli harus berurusan dengan agen properti saat akan transaksi. Ketika disebut Hak Eksklusif, maka ini akan melibatkan seluruh komponen yang ada dalam naskah (teks, gambar, tabel). Dalam kasus makalah non OA, maka secara baku pembaca yang ingin menggunakan gambar dan tabel akan diminta untuk [mengajukan permohonan ke EL](https://www.elsevier.com/about/policies/copyright/permissions) (sebagai pemegang Hak Cipta), disamping penyitiran yang telah dilakukan. 

* **The right to provide the article in all forms and media so the article can be used on the latest technology** even after publication.: hak ini adalah agar EL dapat mengubah makalah anda dari bentuk A ke bentuk B tanpa perlu minta izin lagi kepada anda, misal dulu artikel hanya tersedia dalam bentuk cetak, kemudian teknologi digital PDF muncul, maka EL dapat mengubah format cetak menjadi PDF tanpa perlu izin anda lagi. Baik, sampai level tertentu, kegiatan ini menguntungkan penulis, karena ia tidak perlu lagi melakukan alih media untuk membuat karyanya _up to date_ kepada teknologi, tapi apakah tidak mungkin kalau ini sifatnya opsional?

* **The authority to enforce the rights in the article, on behalf of an author**, against third parties, for example in the case of plagiarism or copyright infringement.: di sini EL ingin membantu anda bila ada penyalahgunaan karya anda, misal plagiarisme. Terlihat menguntungkan bagi penulis, tapi saya sedang mencari (mohon bantuan bila ada yang tahu), berapa kasus plagiarisme yang ditangani penerbit? Dan sampai ke mana mereka akan membantu kita dalam ranah hukum? Saya tidak terlalu yakin itu banyak dilakukan. Mungkin saja ada, tetapi mungkin itu hanya untuk artikel yang berprofil tinggi (_high profile_) atau yang berkaitan dengan teknologi yang dipatenkan.

Kondisi yang berlaku untuk SN secara umum dapat dibaca di sini. 

>Like many other scientific publishers Springer requires authors to transfer the copyright prior to publication for subscription articles. This permits Springer to reproduce, publish, distribute and archive the article in print and electronic form and to defend against improper use of the article. By signing the Copyright Transfer Statement you still retain substantial rights, such as self-archiving. ([Referensi](https://www.springer.com/gp/open-access/publication-policies/copyright-transfer))

Kondisi umum pengalihan Hak Cipta untuk TF ada di sini.

>To publish an article and make it available, we need publishing rights from you for that work. We therefore ask authors publishing in one of our journals to sign an author contract which grants us the necessary publishing rights. This will be after your manuscript has been through the peer-review process, been accepted and moves into production. Our Production team will then send you an email with all the details. ([Referensi](https://authorservices.taylorandfrancis.com/copyright-and-you/))

### Hak yang tertinggal di penulis

* `Share their article for Personal Use, Internal Institutional Use and Scholarly Sharing purposes, with a DOI link to the version of record on ScienceDirect (and with the Creative Commons CC-BY-NC- ND license for author manuscript versions)`: membagikan artikel dengan cara yang dibatasi.
* `Retain patent, trademark and other intellectual property rights (including research data)`: hak atas paten, merek, dan data riset.
* `Proper attribution and credit for the published work`: hak untuk disitir.

## CTA untuk rute OA

Klausul untuk jurnal non OA sedikit berbeda. Penerbit tidak meminta Hak Cipta secara eksklusif, tetapi hanya hak untuk mempublikasikannya (meletakkan makalah di laman resmi jurnal). Di sini, penulis tetap memiliki Hak Ciptanya dan memberikan Hak Penerbitan (_Publishing rigts_) kepada EL. Kemudian EL akan merilis makalah-makalah kepada pembaca dengan lisensi [Creative Commons](creativecommons.org), misal CC-BY atau CC-BY-NC, dst. Referensi tentang Hak Cipta rute OA EL dapat di baca dalam bab OA [di sini](https://www.elsevier.com/about/policies/copyright), untuk SN https://www.springer.com/gp/open-access/springer-open-choice
, dan TF [di sini](https://authorservices.taylorandfrancis.com/copyright-and-you/) (bab OA articles).

# Apa saja opsi penulis

Sekarang opsi apa saja yang dimiliki penulis dan apa yang perlu dilakukannya untuk meminimkan dampak.

* **Bila penulis tidak punya biaya publikasi (APC)**. Penulis punya dua opsi: **(1)** **memilih jurnal non OA** dengan segala resikonya di atas. Penulis diberikan peluang untuk mengunggah manuskrip versi preprint atau post print (versi _accepted_ tapi masih dalam format mentah) ke repositori nirlaba (bukan ResearchGate dan Academia.Edu). Dengan melakukan ini, maka pembaca akan punya pilihan untuk membaca naskah anda secara lengkap, tanpa harus membayar USD 20-30 per makalah. Bila anda mengunggah versi preprint atau post print, maka pembaca juga dapat menggunakan seluruh gambar dan tabel yang ada di dalamnya versi itu tanpa harus meminta izin kepada penerbit.  Atau **(2) memilih jurnal OA yang tidak menarik biaya APC**. Apakah anda tahu fakta bahwa ada 70% lebih jurnal OA yang tidak menarik biaya APC ([data DOAJ](https://doaj.org))? Anda bisa pilih salah satunya yang paling sesuai dengan lingkup makalah. Tapi ada yang perlu anda perhatikan. Tidak semua jurnal OA itu memiliki kriteria prestise yang disyaratkan oleh negara. 
* **Bila penulis memiliki biaya publikasi (APC)**. Penulis bisa langsung memilih jurnal OA sesuai kemampuannya. Saran saya, tidak perlu mengejar jurnal dengan APC mahal, kecuali bila anda mengejar prestise. Biasanya jurnal dengan APC mahal dikelola oleh penerbit komersial besar yang telah stabil model bisnisnya dan memiliki kuartil jumlah sitasi Scimago dan JIF yang memenuhi kriteria prestise yang disyaratkan negara (baca [artikel tentang COVID ini sebagai ilustrasi](https://figshare.com/articles/The_rapid_publications_on_COVID_An_open_science_perspective/12084339)).

# Penutup

Semoga sampai di sini sudah makin jelas beberapa hal yang perlu diperhatikan dosen dan peneliti Indonesia secara umum, sebelum menandatangani formulir CTA. Kalaupun pada akhirnya anda tandatangani, lakukan dengan penuh kesadaran bahwa jelas anda telah melakukan kesalahan dan langkah itu anda lakukan karena terpaksa (untuk memenuhi persyaratan negara). Ketika anda memilih rute OA, maka anda telah membuat kesalahan dengan memberikan karya anda kepada "orang lain" yang berperan minimum. Saat anda memilih untuk membayar uang APC pun, anda masih melakukan kesalahan, yaitu anda membayar "orang lain" untuk melakukan pekerjaan penerbitan yang sebenarnya dapat anda lakukan sendiri dengan [modus Green OA (via repositori)](https://www.youtube.com/watch?v=P5UqNmjOiWg&t=30s). Jadi sebenarnya opsi manapun yang anda pilih, maka [yang paling banyak mengeluarkan modal adalah negara tapi yang paling banyak menerima keuntungan adalah penerbit](https://medium.com/open-science-indonesia/rute-publikasi-siapa-yang-membayar-dan-siapa-yang-diuntungkan-a16192850fff).

Selamat menjalankan ibadah puasa Ramadhan 2020.

[@dasaptaerwin](twitter.com/dasaptaerwin)