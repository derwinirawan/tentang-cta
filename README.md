# Tentang CTA

Repositori ini menyimpan naskah dan material pendukung tentang CTA (copyright transfer agreement). Masalah ini jarang dibahas, padahal konsekuensinya sangat berat. Semoga tulisan ini membuat lebih banyak penulis menyadari apa yang ditandatanganinya. 

Versi html dapat dibaca di blog dasaptaerwin.net, versi pdf, docx, dan odt (open/libre office) dapat diunduh dari [repositori ini](https://gitlab.com/derwinirawan/tentang-cta).

Lisensi seluruh dokumen: [CC-0 public domain](https://creativecommons.org/publicdomain/zero/1.0/)

Salam,

[@dasaptaerwin](twitter.com/dasaptaerwin)

